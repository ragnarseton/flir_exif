# FLIR image viewer

## Description
Extract and view thermal images produced by FLIR thermal cameras. Additionally you can use one or more reference image to approximate the temperatures in one or more ROIs in "thermal videos" recorded with a FLIR one pro (and possibly others, i haven't tried tho).

## Usage instructions
Download [flir.m](https://gitlab.com/ragnarseton/flir_exif/-/raw/master/flir.m?inline=false) and run ``help flir`` from the matlab REPL. You need to have [ExifTool](https://exiftool.org/) available to call (no installation, so no admin privileges, needed tho).

## Screenshots
![Screenshot](screenshot.png)
FLIR image and thermal data shown side by side.

## Acknowledgments
The tool is entirely based on [ExifTool](https://exiftool.org/) by Phil Harvey, that's the tool that does all the heavy lifting.

## Compatibility
Tested with MATLAB R2022a and b as well as R2023a.

## License
[BSD-2-Clause](https://opensource.org/license/bsd-2-clause/)


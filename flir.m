classdef flir
% flir image and video utility class for the hotBlood project
%
% NOTES ON FLIR ONE PRO:
% As you may know the FLIR one pro embeds a "regular" image taken by the phone
% camera in its thermal images. Unfortunately the distance between the thermal
% camera and the phone camera makes the images have different perspectives and
% they are therefore not trivially aligned, hence i've left out functionality
% that includes these "regular" images.
%
% NOTES ON STABILITY:
% I've only tested this with jpg-images and mp4-files from a FLIR one pro. If
% it doesn't work with your images poke around a bit in the source or send an
% email: ragnar.seton@uit.no
% 
% REQUIREMENTS:
% - matlab >= R2019b (i think)
% - image processing toolbox
% - curve fitting toolbox
% - exiftool, a fantastic tool from Phil Harvey at https://exiftool.org
%
% exiftool instructions for windows (no installation needed):
% 1) download the windows executable (not installer) from https://exiftool.org
% 2) unpack the zip-file in a persistant location.
% 3) rename file from exiftool(-k).exe to just ëxiftool.exe (if you can't see
%    the ".exe" part of the filename just remove the "(-k)"-part)
% 4) change the EXIFTOOL_EXE property below
%
% TODO:
% - add some gui helper functions like in the zygo-viewer
% - show_flir_img_pair, func to display the raw image and temp image
%   side-by-side with hover func and corresponding mouse indicator on the other
%   one
% - make the hover functionality into separate funcs instead of copypasta
%
% EXAMPLES:
% fn_img = "C:\Users\USERNAME\flir\image.jpg";
% % display image with thermal information
% flir.show_flir_img(img_fn, 'show_minmax', true);
% % display thermal information as image
% flir.show_flir_temp_img(img_fn, 'show_minmax', true);
%
% fn_vid = "C:\Users\USERNAME\flir\video.mp4";
% % make a "thermal video" from a FLIR one pro video using an image as reference
% TODO

	properties (Constant = true)
		EXIFTOOL_EXE = "exiftool";
	end

	methods (Static = true)
		function md = img2metadata(img, opts)
		% Extract the FLIR-specific metadata required to calculate temperatures
		% from.
		%
		% ARGS:
		% img - file to fetch temps from
		%
		% OPT/NAMED ARGS:
		% exiftool - exiftool executable, i.e. going system(exiftool) should
		%            invoke it.
			arguments
				img {mustBeFile}
				opts.exiftool {mustBeTextScalar} = flir.EXIFTOOL_EXE
			end
			flds = ["PlanckR1", "PlanckR2", "PlanckB", "PlanckO", "PlanckF", ...
					"Emissivity", "ObjectDistance", ...
					"ReflectedApparentTemperature", ...
					"AtmosphericTemperature", "IRWindowTemperature", ...
					"IRWindowTransmission", "RelativeHumidity", ...
					"AtmosphericTransAlpha1", "AtmosphericTransAlpha2", ...
					"AtmosphericTransBeta1", "AtmosphericTransBeta2", ...
					"AtmosphericTransX", "RawThermalImageType", ...
					"ExifByteOrder"];
			% build the string (note that byte order is not a -Flir:* tag, so
			% we'll just skip that prefix entirely
			cmd = opts.exiftool;
			for fld = flds
				cmd = append(cmd, ' -', fld);
			end
			% couldn't figure out how to use readtable on a string so i guess
			% this stupid thing is how we do it
			tmp_fn = tempname();
			% specifying a csv-filename with the -csv-argument didn't work for
			% me on windows with exiftool version 12.48 (it just kept crashing)
			% so we'll just redirect output
			cmd = append(cmd, ' -m -csv "', img, '" > ', tmp_fn);
			[status, ~] = system(cmd);
			assert(status == 0, sprintf('command %s failed with code %d', ...
										cmd, status));
			tbl = readtable(tmp_fn);
			delete(tmp_fn);
			md = struct();
			md.filename = tbl.SourceFile{1};
			md.p = struct();
			md.p.r1 = tbl.PlanckR1;
			md.p.r2 = tbl.PlanckR2;
			md.p.b = tbl.PlanckB;
			md.p.o = tbl.PlanckO;
			md.p.f = tbl.PlanckF;
			md.emissivity = tbl.Emissivity;
			C = textscan(tbl.ObjectDistance{1}, '%f %c');
			md.od = C{1};
			C = textscan(tbl.ReflectedApparentTemperature{1}, '%f %c');
			md.rat = C{1};
			md.temp_unit = C{2};
			C = textscan(tbl.AtmosphericTemperature{1}, '%f %c');
			md.at = C{1};
			C = textscan(tbl.IRWindowTemperature{1}, '%f %c');
			md.IRWt = C{1};
			md.IRW_trans = tbl.IRWindowTransmission;
			C = textscan(tbl.RelativeHumidity{1}, '%f %c');
			md.rh = C{1} / 100;
			md.ata1 = tbl.AtmosphericTransAlpha1;
			md.ata2 = tbl.AtmosphericTransAlpha2;
			md.atb1 = tbl.AtmosphericTransBeta1;
			md.atb2 = tbl.AtmosphericTransBeta2;
			md.atx = tbl.AtmosphericTransX;
			md.rti_type = tbl.RawThermalImageType{1};

			% fetch the raw image
			tmp_fn = append(tempname(), '.', md.rti_type);
			cmd = append(opts.exiftool, ' -b -RawThermalImage "', img, ...
						 '" > ', tmp_fn);
			[status, ~] = system(cmd);
			assert(status == 0, sprintf('command %s failed with code %d', ...
										cmd, status));
			if startsWith(tbl.ExifByteOrder{1}, 'Little', 'IgnoreCase', true)
				md.rti = swapbytes(importdata(tmp_fn));
			else
				if ~startsWith(tbl.ExifByteOrder{1}, 'Big', 'IgnoreCase', true)
					warning(['Unknown exif byte order field (%s), ', ...
							 'assuming big-endian - if you get bad temp ', ...
							 'readings try swapbytes(md.rti)'], ...
							tbl.ExifByteOrder{1});
				end
				md.rti = importdata(tmp_fn);
			end
			delete(tmp_fn);
		end
		
		function T = metadata2temp_img(md)
		% convert the raw thermal image in flir metadata to an "image"
		% containing temperature values in C in each pixel.
		%
		% ARGS:
		% md - metadata from flir.img2metadata
		
			% see https://exiftool.org/forum/index.php/topic,4898.60.html and
			% raw2temp.R from https://github.com/gtatters/Thermimage
			h2o = sqrt(md.rh*exp(1.5587+0.06939*md.at-0.00027816*md.at^2 + ...
								 0.00000068455*md.at^3));
			% radiance at T (given in C)
			radiance = @(T) md.p.r1 / ...
							( md.p.r2 * (exp(md.p.b/(T+273.15))-md.p.f) ) - ...
							md.p.o;

			% atmospheric transmissivity
			a_trans =  md.atx  *exp(-sqrt(md.od/2)*(md.ata1+md.atb1*h2o)) + ...
				     (1-md.atx)*exp(-sqrt(md.od/2)*(md.ata2+md.atb2*h2o));

			% radiance reflected off the object before the window (cam lens)..
			o_rad = radiance(md.rat);
			% ..and its attenuation
			o_ra = o_rad * (1 - md.emissivity) / md.emissivity;

			% atmospheric radiance before the window..
			a_rad = radiance(md.at);
			% ..and its attenuation
			a_ra = a_rad * (1-a_trans) / (md.emissivity * a_trans);

			% radiance at window..
			w_rad = radiance(md.IRWt);
			% ..and its attenuation
			w_ra = w_rad * (1-md.IRW_trans) / ...
				   (md.emissivity * a_trans * md.IRW_trans);

			% attenuation of the radiance reflected off the object _at_ the
			% window. note that in the R package this one uses a reflectivity
			% value for the window.. which to me seems like 1-transmissivity,
			% but they define it as just 0 (anti-reflective coating on the
			% window). effectively it will (likely) be the same result, but i
			% think this here makes more sense.
			ow_ra = o_rad * (1-md.IRW_trans) / ...
					(md.emissivity * a_trans * md.IRW_trans);

			% attenuation of the atmospheric radiance _at_ the window
			aw_ra = a_rad * (1-a_trans) / ...
					(md.emissivity * a_trans * md.IRW_trans);

			% adjusted pixel values
			apx = double(md.rti)./(md.emissivity*md.IRW_trans*a_trans^2) - ...
				  a_ra - aw_ra - w_ra - o_ra - ow_ra;
			% and finally the temperatures
			T = md.p.b ./ ...
				   log(md.p.f + md.p.r1 ./ (md.p.r2.*(apx+md.p.o))) - ...
				   273.15;
			% alternate approach:
%			refl = md.p.r1 / ...
%				   (md.p.r2*(exp(md.p.b/(md.rat+273.15))-md.p.f)) - md.p.o;
%			obj = (px - (1-md.emissivity)*refl)/md.emissivity;
%			T(i) = md.p.b / ...
%				   log(md.p.r1/(md.p.r2*(raw_obj+md.p.o))+md.p.f) - 273.15;
		end
		
		function TI = img2temp_img(img, opts)
		% ARGS:
		% img - file to fetch temps from
		%
		% OPT/NAMED ARGS:
		% exiftool - exiftool executable, i.e. going system(exiftool) should
		%            invoke it.
			arguments
				img {mustBeFile}
				opts.exiftool {mustBeTextScalar} = flir.EXIFTOOL_EXE
			end
			o = namedargs2cell(opts);
			TI = flir.metadata2temp_img(flir.img2metadata(img, o{:}));
		end
		
		function show_temp_img(TI, opts)
		% Display temperature image from flir.img2temp_img or
		% flir.metadata2temp_img with a hover function to view temps.
		%
		% ARGS:
		% TI - temperature image
		%
		% OPT/NAMED ARGS:
		% show_minmax - if true min and max temperatures are also plotted.
		%
		% TODO:
		% - more click-actions:
		%	- rightclick in img should only copy temps
		%	- middleclick in img should only copy position
		%	- add all three click actions for ylabel (but there just copy
		%	  min/max values)
		%   - modifier (shift maybe) clicks should copy usable matlab code,
		%     e.g. sprintf("struct('temp', %f, 'row', %d, 'col', %d);", T, r,
		%     c).
		% - document the clicks in the help section
			arguments
				TI (:,:) {mustBeNumeric}
				opts.show_minmax (1,1) {mustBeNumericOrLogical} = false
			end
			f = figure();
			ah = axes('Parent', f);
			h = imagesc(TI, 'Parent', ah);
			colorbar(ah);
			xlabel(ah, 'Hover image to see temp here');
			isz = size(TI);
			[lo, i] = min(TI, [], 'all', 'linear');
			[lor, loc] = ind2sub(isz, i);
			[hi, i] = max(TI, [], 'all', 'linear');
			[hir, hic] = ind2sub(isz, i);
			ylabel(ah, sprintf('min: %.2f C [%d, %d], max: %.2f C [%d, %d]', ...
							    lo, lor, loc, hi, hir, hic));
			if opts.show_minmax
				hold(ah, 'on');
				flir.outlined_x(ah, loc, lor, 'b', 'w');
				flir.outlined_x(ah, hic, hir, 'r', 'w');
%				plot(ah, loc, lor, 'xw', hic, hir, 'xk', ...
%					 'LineWidth', 1.5, 'MarkerSize', 9);
			end
			title(ah, 'Hover to see temps, click to copy');
			set(ah, 'DataAspectRatio', [1,1,1]);
			f.WindowButtonMotionFcn = @mouseMove;
			h.ButtonDownFcn = @mouseClick;
			function mouseMove(~,~)
				p = round(ah.CurrentPoint(1,1:2));
				if 1 <= p(1) && p(1) <= isz(2) && 1 <= p(2) && p(2) <= isz(1)
					ah.XAxis.Label.String = sprintf('%.2f C [%d, %d]', ...
													TI(p(2),p(1)), p(2), p(1));
				end
			end
			function mouseClick(~, v)
				p = round(v.IntersectionPoint(1:2));
				s = sprintf('%.2f C [%d, %d]', TI(p(2),p(1)), p(2), p(1));
				clipboard('copy', s);
				ah.XAxis.Label.String = append(s, ' copied to clipboard');
			end
		end

		function varargout = show_flir_temp_img(img, opts)
		% Display FLIR image with temperatures as pixel values. Wrapper for
		% flir.show_temp_image.
		%
		% ARGS:
		% img - file to display
		%
		% OPT/NAMED ARGS:
		% exiftool    - exiftool executable, i.e. going system(exiftool) should
		%               invoke it.
		% show_minmax - if true min and max temperatures are also plotted.
		%
		% OPT RETURNS:
		% TI - temperature image
		% md - FLIR metadata required to calculate temperatures
			arguments
				img {mustBeFile}
				opts.exiftool {mustBeTextScalar} = flir.EXIFTOOL_EXE
				opts.show_minmax (1,1) {mustBeNumericOrLogical} = false
			end
			md = flir.img2metadata(img, 'exiftool', opts.exiftool);
			TI = flir.metadata2temp_img(md);
			flir.show_temp_img(TI, 'show_minmax', opts.show_minmax);
			if nargout > 0
				varargout{1} = TI;
				if nargout > 1; varargout{2} = md; end
			end
		end

		function varargout = show_flir_img(img, opts)
		% Display (monochome) FLIR image with a hover function to view temps. To
		% display only the temperature data see show_flir_temp_img or
		% show_temp_img.
		%
		% ARGS:
		% img - file to display
		%
		% OPT/NAMED ARGS:
		% exiftool		- exiftool executable, i.e. going system(exiftool)
		%				  should invoke it.
		% show_minmax	- if true min and max temperatures are also plotted.
		% show_colorbar	- if true a "colorbar" will be generated from the image.
		%
		% OPT RETURNS:
		% TI - temperature image
		% md - FLIR metadata required to calculate temperatures
			arguments
				img {mustBeFile}
				opts.exiftool {mustBeTextScalar} = flir.EXIFTOOL_EXE
				opts.show_colorbar (1,1) {mustBeNumericOrLogical} = true
				opts.show_minmax (1,1) {mustBeNumericOrLogical} = false
			end
			md = flir.img2metadata(img, 'exiftool', opts.exiftool);
			TI = flir.metadata2temp_img(md);
			if nargout > 0
				varargout{1} = TI;
				if nargout > 1; varargout{2} = md; end
			end

			f = figure();
			ah = axes('Parent', f, 'Units', 'normalized', ...
					  'Position', [0.07 0.07 0.87 0.85]);
			h = imshow(img, 'Parent', ah);
			xlabel(ah, 'Hover image to see temp here');
			isz = size(TI);
			[lo, i] = min(TI, [], 'all', 'linear');
			[lor, loc] = ind2sub(isz, i);
			[hi, i] = max(TI, [], 'all', 'linear');
			[hir, hic] = ind2sub(isz, i);
			ylabel(ah, sprintf('min: %.2f C [%d, %d], max: %.2f C [%d, %d]', ...
							    lo, lor, loc, hi, hir, hic));
			if opts.show_minmax
				hold(ah, 'on');
				flir.outlined_x(ah, loc, lor, 'b', 'w');
				flir.outlined_x(ah, hic, hir, 'r', 'w');
			end
			if opts.show_colorbar
				flir.imshow_colorbar(ah, TI, 'img', h.CData);
			end
			title(ah, 'Hover to see temps, click to copy');
			%set(ah, 'DataAspectRatio', [1,1,1]);
			f.WindowButtonMotionFcn = @mouseMove;
			h.ButtonDownFcn = @mouseClick;
			function mouseMove(~,~)
				p = round(ah.CurrentPoint(1,1:2));
				if 1 <= p(1) && p(1) <= isz(2) && 1 <= p(2) && p(2) <= isz(1)
					ah.XAxis.Label.String = sprintf('%.2f C [%d, %d]', ...
													TI(p(2),p(1)), p(2), p(1));
				end
			end
			function mouseClick(~, v)
				p = round(v.IntersectionPoint(1:2));
				s = sprintf('%.2f C [%d, %d]', TI(p(2),p(1)), p(2), p(1));
				clipboard('copy', s);
				ah.XAxis.Label.String = append(s, ' copied to clipboard');
			end
		end
		
		function [px2temp, varargout] = make_lookup_table(opts)
		% makes a lookup table that converts pixel values to temperatures from
		% pairs of FLIR intensity (grayscale) images and temperature images.
		% since most FLIR images contain 3 channels the images are converted by
		% averaging the channels (or you supply the grayscale images yourself
		% with img).
		% 
		% OPT/NAMED ARGS:
		% file     - filename(s) of FLIR image(s) to use as intensity
		%            (greyscale) and/or temp image(s). it can thus be used on
		%            its own, but if you have already extracted the metadata it
		%            would make sense to provide that ('md') as well. note that
		%            if your image(s) has multiple color channels they will just
		%            be averaged to make the grayscale(s).
		% exiftool - has to be set if 'file' is used for temp image (i.e. it is
		%            used alone or together with 'img') and exiftool can not be
		%            invoked by just calling system("exiftool").
		% md       - metadata structure(s) from flir.img2metadata. can be used
		%            together to make the temp image with 'filename' or 'img'.
		% TI       - temp image from flir.img2temp_img or
		%            flir.metadata2temp_img. can be used instead of
		%            'md'.
		% img      - grayscale (2D matrix) image(s). can be used together with
		%            'md', 'TI', or 'file' (for the last only the metadata will
		%            will be fetched from the file).
		% i_confl  - strategy to use if there are multiple intensities for the
		%            same temperature. possible values are:
		%            'low'    - map the lowest conflicting intensity to the
		%                       temperature and replace all conflicting values
		%                       with it
		%            'high'   - map the highest conflicting intensity
		%            'mean'   - map the average of the conflicting intensities
		%            'error'  - don't map anything and throw an error
		%            default value: 'mean'
		% T_confl  - strategy to use if there are multiple temperatures for the
		%            same intensity. possible values are:
		%            'low'    - map the lowest conflicting temperature to the
		%                       intensity and replace all conflicting values
		%                       with it
		%            'high'   - map the highest conflicting temp
		%            'mean'   - map the average of the conflicting temp
		%            'error'  - don't map anything and throw an error
		%            default value: 'mean'
		% norm     - normalize images (divide by intmax(class(img))). useful if
		%            you have FLIR images of different types. possible values:
		%            true or false, false is default.
		% crop     - Nx2 or Nx4 where N is the number image pairs and in the
		%            columns are the sizes to crop to, either as
		%            [n_rows, n_cols] or [o_row, o_col, n_rows, n_cols], where
		%            n_rows/cols are the number of rows/cols (height/width) and
		%            o_row/col is the starting offset, so [W,H] is the same as
		%            [1,1, W,H]. useful when you need to crop out the FLIR
		%            watermark.
		%
		% RETURNS:
		% px2temp - function that takes a pixelvalue (in the same range as that
		%           of img) as input and returns a temperature.
		%
		% OPT RETURNS:
		% x,v - values that can be used with interp1, spline, or similar
		%       interpolation functions
		%
		% EXAMPLES:
		% % parse directly from a flir jpg
		% px2temp = make_lookup_table('file', 'flir.jpg');
		%
		% % parse directly from a flir jpg, providing the exiftool
		% px2temp = make_lookup_table('file', 'flir.jpg' ...
		%                             'exiftool', 'C:/exiftool.exe');
		%
		% TODO:
		% - extrapolation strategy (use ref min/max, provided min/max, other)
		% - more examples
			arguments
				opts.file {mustBeText} = ''
				opts.exiftool {mustBeTextScalar} = flir.EXIFTOOL_EXE
				opts.md struct = struct([])
				opts.TI {mustBeNumeric} = []
				opts.img {mustBeNumeric} = []
				opts.i_confl char {mustBeMember(opts.i_confl,{'low', 'high', ...
									'mean', 'error'})} = 'mean'
				opts.T_confl char {mustBeMember(opts.T_confl,{'low', 'high', ...
									'mean', 'error'})} = 'mean'
				opts.norm logical = false
				opts.crop (:,:) = []
			end
			
			% parse the input
			if ~isempty(opts.img)
				if iscell(opts.img); img = opts.img;
				else; img = num2cell(opts.img, [1,2]);
				end
			else
				assert(~isempty(opts.file), ...
					   "Either 'file' or 'img' has to be provided");
				if ischar(opts.file); opts.file = string(opts.file); end
				img = cell(numel(opts.file), 1);
				for i = 1:numel(opts.file)
					if iscell(opts.file); fn = opts.file{i};
					else; fn = opts.file(i);
					end
					img{i} = importdata(fn);
					assert(ndims(img{i}) < 4, ...
						   sprintf('"%s" has too many dimensions (%d)', ...
								   fn, ndims(img{i})));
					if ndims(img{i}) == 3
						img{i} = mean(img{i}, 3);
					end
					
					if opts.norm && isinteger(img{i})
						img{i} = double(img{i})./double(intmax(class(img{i})));
					end
				end
			end
			if ~isempty(opts.TI)
				if iscell(opts.TI); TI = opts.TI;
				else; TI = num2cell(opts.TI, [1,2]);
				end
			elseif ~isempty(opts.md)
				TI = cell(length(opts.md), 1);
				for i = 1:length(t)
					TI{i} = flir.metadata2temp_img(opts.md(i));
				end
			else
				assert(~isempty(opts.file), ...
					   "Either 'TI', 'md', or 'file' has to be provided");
				TI = cell(numel(opts.file), 1);
				for i = 1:numel(opts.file)
					if iscell(opts.file); fn = opts.file{i};
					else; fn = opts.file(i);
					end
					TI{i} = flir.img2temp_img(fn, 'exiftool', opts.exiftool);
				end
			end
			assert(numel(TI) == numel(img), ...
				   'You need the same number of grayscales and temp images');
			if ~isempty(opts.crop)
				if size(opts.crop,1) == 1 && numel(TI) > 1
					crop = repmat(opts.crop, numel(TI), 1);
				else
					crop = opts.crop;
				end
				if size(crop,2) == 2
					sr = ones(size(crop,2), 1);
					sc = sr;
				elseif size(crop,2) == 4
					sr = crop(:,1);
					sc = crop(:,2);
				else
					error('crop can only be Nx2 or Nx4');
				end
				for i = 1:numel(TI)
					TI{i} = TI{i}(sr:crop(i,end-1), sc:crop(i,end));
					img{i} = img{i}(sr:crop(i,end-1), sc:crop(i,end));
				end
			end

			n = numel(TI);
			px = cell(n, 1);
			temp = cell(n, 1);
			for i = 1:n
				assert(all(size(img{i}) == size(TI{i})), ...
					   sprintf('img and TI %d differ in size', i)); 
				% discard any duplicate pixel values, this assumes that all
				% temps in one TI has been calced with the same settings, which
				% seems like a safe assumption =)
				[px{i}, idx, ~] = unique(img{i}); % unique returns them sorted
				temp{i} = TI{i}(idx);
			end
			for i = 1:(n-1)
				% check for intensity conflicts
				for j = (i+1):n
					[~, idxi, idxj] = intersect(px{i}, px{j});
					if isempty(idxi); continue;
					elseif all(temp{i}(idxi) == temp{j}(idxj)); continue;
					end
					repl = [reshape(px{i}(idxi),1,[]); ...
							reshape(px{j}(idxj),1,[])];
					switch lower(opts.i_confl)
						case 'low'
							repl = min(repl);
						case 'high'
							repl = max(repl);
						case 'mean'
							repl = mean(repl);
						case 'error'
							error('%d intensity conflicts in img %d and %d', ...
								  numel(repl), i, j);
					end
					px{i}(idxi) = repl;
					px{j}(idxj) = repl;
				end
				% check for temp conflicts
				for j = (i+1):n
					[~, idxi, idxj] = intersect(temp{i}, temp{j});
					if isempty(idxi); continue;
					elseif all(px{i}(idxi) == px{j}(idxj)); continue;
					end
					repl = [reshape(temp{i}(idxi),1,[]); ...
							reshape(temp{j}(idxj),1,[])];
					switch lower(opts.T_confl)
						case 'low'
							repl = min(repl);
						case 'high'
							repl = max(repl);
						case 'mean'
							repl = mean(repl);
						case 'error'
							error('%d intensity conflicts in img %d and %d', ...
								  numel(repl), i, j);
					end
					temp{i}(idxi) = repl;
					temp{j}(idxj) = repl;
				end
			end
			% merge and return
			xm = vertcat(px{:});
			vm = vertcat(temp{:});
			[x, xi, ~] = unique(xm);
			% made this explicit just to remember that interp* only deals with
			% floats
			if ~isfloat(x); x = double(x); end
			if ~isfloat(vm); v = double(vm(xi));
			else; v = vm(xi);
			end
			px2temp = @(xq) interp1(x, v, xq, 'spline');
			if nargout == 3
				varargout{1} = x;
				varargout{2} = vm(xi);
			end
		end

		function [T, varargout] = vid_temps(v, tpr, tpc, px2temp, opts)
		% ARGS:
		% v       - video as HxWxDxN matrix, where H is height in number of
		%           rows, W width, D the number of color channels, and N number
		%           of frames. note that D is needed even if it is 1.
		% tpr     - temperature point(s) row index
		% tpc     - temperature point(s) column index
		% px2temp - function that converts intensity values to temperatures, see
		%           flir.make_lookup_table
		%
		% OPT/NAMED ARGS:
		% radius - radius of the temp zones
		%
		% OPT RETURN:
		% T_max - max temperatures of temp zones
		%
		% TODO:
		% - make it possible to have different sized ROIs
		% - clean up and properly document the last part
			arguments
				v {mustBeNumeric}
				tpr, tpc (1,:) {mustBeNumeric}
				px2temp function_handle
				opts.radius (1,1) {mustBeNumeric} = 0
				% TODO maybe add different masks
			end
			nf = size(v,4);
			ntp = length(tpr);
			% # rows  = number of color channels
			% # cols  = number of temp points (or zones)
			% # layrs = number of frames
			px = zeros(size(v,3), ntp, nf);
			if opts.radius == 0 % just fetch the pixels
				for i = 1:ntp; px(:, i, :) = v(tpr(i), tpc(i), :, :); end
			else
				% generate the mask
				im = flir.mask_circle(size(v,1), size(v,2), tpr, tpc, ...
									  opts.radius, 'indices', true);
				npx_tz = size(im,1)/ntp; % number of pixels per temp zone
				% start by "splitting" channels into frames
				nnf = size(v,3)*nf; % update number of frames
				nv = reshape(v, size(v,1), size(v,2), nnf); % new video
				% then we generate our chonker index array
				fi = reshape(repmat(1:nnf, size(im,1), 1), [], 1); % frame idxs
				im = repmat(im, nnf, 1); % index mask for each frame
				mpx = nv(sub2ind(size(nv), im(:,1), im(:,2), fi));
				% mpx now contains [R1_TZ1, R1_TZ2, G1_TZ1, G1_TZ2, B1_TZ1, ...
				% B1_TZ2, R2_TZ1, ...], where Xn_TZm is all pixel vals for color
				% channel X in frame n in temp zone m. so we reshape it to hold
				% one color channel for one temp zone and frame in each column
				% and take the mean of it
%				mmpx = mean(reshape(mpx, npx_tz, []));
				% this one converts to temps first before any averaging
				all_T = px2temp(double(mpx));
				if nargout > 1 && opts.radius > 0
					all_max_T = max(reshape(all_T, npx_tz, []));
					varargout{1} = zeros(nf, ntp);
				end
				mmpx = mean(reshape(all_T, npx_tz, []));
				% finally we reconstruct an averaged DxKxN matrix where D is the
				% # color channels, K the # temp zones, and N the # frames
				for i = 1:ntp
					px(:, i, :) = reshape(mmpx(i:ntp:end), size(v,3), []); 
					if nargout > 1 && opts.radius > 0
						varargout{1}(:, i) = mean(reshape(all_max_T(i:ntp:end), size(v,3), []));
					end
				end
			end
%			T = px2temp(squeeze(mean(px))');
			T = squeeze(mean(px))';
		end

		function mask = mask_circle(h, w, cr, cc, r, opts)
		% ARGS:
		% h	  - height of image (number of rows)
		% w	  - width of image (number of columns)
		% cr  - mask center row(s) index
		% cc  - mask center column(s) index
		% r   - radius of mask
		% 
		% OPT/NAMED ARGS:
		% indices - return a Nx2 matrix with [row, column] indices instead of a
		%           logical mask. Nx2 is used instead of linear ones in case you
		%           wanna use them with several layers 
		%
		% NOTE:
		% an indices "mask" can be used with sub2ind to produce a logical one in
		% the follwing way:
		% l_mask = flir.mask_circle(args..); % reference logical mask
		% i_mask = flir.mask_circle(args.., 'indices', true); % indices
		% i2l_mask = false(size(l_mask)); % indices to logical mask
		% % add the actual mask part:
		% i2l_mask(sub2ind(size(i2l_mask), i_mask(:,1), i_mask(:,2))) = true;
		% assert(all(i_mask == i2l_mask, 'all')); % test
		%
		% TODO:
		% - deal with c/r_dist >= cc/r
		% - deal with r < 1
		% - support per temp point radius, note that the format for indices
		%   masks gotta change then tho since you can't just divide the length
		%   of the returned indices with number of temp points to get the
		%   individual temp point indices
		% - make the generic ellipse one instead (setting r_dist = c_dist' if
		%   rr(1) == rr(2)).
			arguments
				h, w (1,1) {mustBeNumeric}
				cr, cc (1,:) {mustBeNumeric}
				r (1,1) {mustBeNumeric}
				opts.indices (1,1) {mustBeNumericOrLogical} = false
			end
			% distance from center in columns
			spn = (-r+1):(r-1);
			% rep into a matrix
			c_dist = repmat(spn.^2, length(spn), 1);
			% for a circle r_dist is just c_dist', the r^2-1 makes small radii
			% look better (cuts the corners of r = 3)
			circ_mask = (c_dist + c_dist') < r^2-1;
			
			if opts.indices
				[mr, mc] = find(circ_mask);
				mask = zeros(length(mr)*length(cr), 2);
				for i = 1:length(cr)
					mask((i-1)*length(mr)+(1:length(mr)), :) = ...
						[cr(i)-r + mr, cc(i)-r + mc];
				end
			else
				mask = false(h, w);
				for i = 1:length(cr)
					mask(cr(i)+spn, cc(i)+spn) = circ_mask;
				end
			end
		end

		function mask = mask_rect(h, w, cr, cc, sz_r, sz_c)
		% see mask_circle for args
		% TODO:
		% - add indices support
		% - deal with dr >= cr
		% - add possibility to have different sz for the the different
		%	centers
		% - use offset in given direction instead of decreasing sz
			mask = false(h, w, length(cr));
			zs = @(x) -x:x; % zerospan
			if mod(sz_r, 2) == 0
				warning('Reducing row-masksize to odd size')
				sz_r = sz_r - 1;
			end
			dr = floor(sz_r/2);
			if mod(sz_c, 2) == 0
				warning('Reducing column-masksize to odd size')
				sz_c = sz_c - 1;
			end
			dc = floor(sz_c)/2;
			for i = 1:length(cr)
				mask(cr(i)+zs(dr), cc(i)+zs(dc), i) = true;
			end
		end
	end
	methods(Static = true, Hidden = true)
		function outlined_x(p, x, y, i_clr, o_clr, opts)
			arguments
				p matlab.graphics.axis.Axes
				x,y (1,1) {mustBeReal}
				i_clr, o_clr {flir.mustBeColor(i_clr, o_clr)}
				opts.i_opts cell = {'LineWidth', 0.5, 'MarkerSize', 9}
				opts.o_opts cell = {'LineWidth', 2, 'MarkerSize', 10}
			end
			plot(p, x, y, 'x', 'MarkerEdgeColor', o_clr, opts.o_opts{:});
			plot(p, x, y, 'x', 'MarkerEdgeColor', i_clr, opts.i_opts{:});
		end
		function tf = mustBeColor(varargin)
			tf = all(cellfun(@(v) ( ...
					(isstring(v) || ischar(v)) && ...
						( ...
							(length(v) == 1 && ismember(char(v), 'rgbcmykw')) || ...
							strcmp(v, "none") ...
						) ...
					) || ( ...
						isreal(v) && all(size(v) == [1,3]) && all((0 <= v) & (v <= 1)) ...
					), ...
				varargin));
		end
		function cb = imshow_colorbar(ah, TI, opts)
		% makes a colorbar for an imshow()ed image
			arguments
				ah (1,1) matlab.graphics.axis.Axes
				TI (:,:) {mustBeNumeric,mustBeReal}
				opts.n_ticks (1,1) {mustBePositive} = 5
				opts.only_min_max (1,1) {mustBeNumericOrLogical} = true
				opts.font_size (1,1) {mustBePositive} = ah.XLabel.FontSize
				opts.font_units {mustBeTextScalar} = ah.XLabel.FontUnits
				opts.img (:,:,:) {mustBeReal} = []
			end
			if ~isempty(opts.img)
				img = opts.img;
			else
				% TODO: do this better
				img = [];
				for c = reshape(ah.Children, 1, [])
					if isa(c, 'matlab.graphics.primitive.Image')
						img = c.CData;
						break;
					end
				end
				assert(~isempty(img), 'Failed to find image in provided handle');
			end
			assert(ndims(img) < 4, 'Only 2 or 3 dim images are supported');
			assert(all(size(img, [1,2]) == size(TI)), ...
				   'Size of img and TI does not match.');
			[uT, i_m2u, ~] = unique(TI);
			%[ir, ic] = ind2sub(size(TI), i_m2u);
			if ismatrix(img); upx = img(i_m2u);
			else
				apx = reshape(img, [], size(img,3));
				upx = apx(i_m2u);
			end
			uT = flipud(uT);
			upx = flipud(upx);

			cb = axes('Parent', ah.Parent, 'Tag', 'flir_colorbar');
			imshow(upx, 'Parent', cb);
 			% aspect ratio mode has to be reset before resizing
			cb.DataAspectRatioMode = 'auto';
			ahu = ah.Units;
			ah.Units = 'normalized';
			p = ah.Position;
			ah.Units = ahu;

			ew = 1-p(1)-p(3);
			set(cb, 'Units', 'normalized', ...
				'InnerPosition', [1-ew, p(2), ew/2, p(4)]);
			if (opts.only_min_max)
				text(0.5, 0, sprintf('%.2f', uT(end)), ...
					 'FontSize', opts.font_size, ...
					 'HorizontalAlignment', 'center', ...
					 'VerticalAlignment', 'top', 'Units', 'normalized');
				text(0.5, 1, sprintf('%.2f', uT(1)), ...
					 'FontSize', opts.font_size, ...
					 'FontUnits', opts.font_units, ...
					 'HorizontalAlignment', 'center', ...
					 'VerticalAlignment', 'bottom', 'Units', 'normalized');
			else
				% TODO: fix this to handle extents of the fontsize and whatnot
				yt = linspace(0.5, numel(uT)+0.5, opts.n_ticks);
				ytl = interp1([0.5, 1:numel(uT), numel(uT)+0.5], ...
						  	[uT(1); uT; uT(end)], yt, 'spline');
				set([cb.XAxis, cb.YAxis], 'Visible', 'on');
				set(cb, 'YAxisLocation', 'right', 'XTick', [], 'YTick', yt, ...
					'YTickLabel', sprintfc('%.2f', ytl), 'TickDir', 'in', ...
					'FontSize', opts.font_size, 'Box', 'on');
			end
		end
	end
end
